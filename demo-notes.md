
# Demo notes

## Présentation de App Engine

### Code 

* Application UI
* Application Search

### Control Plane

* https://console.cloud.google.com/appengine?project=i-portfolio-234111
* https://console.cloud.google.com/appengine/services?project=i-portfolio-234111

### Do some requests:

* curl http://search.i-portfolio-234111.appspot.com
* curl http://i-portfolio-234111.appspot.com/

### Lets deploy on AppEngine

* Change the version in maven `search:123`
* Change the message and version in `app.yaml:7` ( Wonderful Demo )
* mvn -f search/pom.xml clean package -P app-engine appengine:deploy; and watch curl -qs https://0-0-2-dot-search-dot-i-portfolio-234111.appspot.com/
 
### Lets show what app-engine provides to us

* Logs: https://console.cloud.google.com/logs/viewer?project=i-portfolio-234111&resource=gae_app&minLogLevel=0&expandAll=false&timestamp=2019-03-14T20:16:06.844000000Z&customFacets=&limitCustomFacetWidth=true&dateRangeStart=2019-03-14T19:16:07.095Z&dateRangeEnd=2019-03-14T20:16:07.095Z&interval=PT1H&logName=projects%2Fi-portfolio-234111%2Flogs%2Fstdout&logName=projects%2Fi-portfolio-234111%2Flogs%2Fstderr&logName=projects%2Fi-portfolio-234111%2Flogs%2Fappengine.googleapis.com%252Frequest_log&scrollTimestamp=2019-03-14T20:14:18.071000000Z
* Tracing: https://console.cloud.google.com/traces/traces?project=i-portfolio-234111
* Debug - LogPoint 
    * Add logpoint in `search` at line 39 with value: 
        I can add a log message here for {event}
    * curl -k https://search.i-portfolio-234111.appspot.com
    * Open Logs tab
* Debug - Snapshot
    * Add snapshot in `search` at line 40
    * curl -k https://search.i-portfolio-234111.appspot.com
    * Open menu in right part

### Let's control the traffic

* Go to https://console.cloud.google.com/appengine/versions?project=i-portfolio-234111&serviceId=search&versionssize=50
* curl https://0-0-1-dot-search-dot-i-portfolio-234111.appspot.com/
* curl https://0-0-2-dot-search-dot-i-portfolio-234111.appspot.com/

* Select all version and mitigate traffic

* watch -d -n .5 curl -qs https://i-portfolio-234111.appspot.com/

## Présentation de GKE

### Code

Still the same ❤️

### Create a cluster 

Very simple: https://console.cloud.google.com/kubernetes/add?project=i-portfolio-234111&template=standard-cluster

With CLI: 
gcloud beta container --project "i-portfolio-234111" clusters create "k8s-demo-cluster" --zone "us-central1-a" --no-enable-basic-auth --cluster-version "1.12.5-gke.10" --machine-type "n1-standard-4" --image-type "COS" --disk-type "pd-ssd" --disk-size "100" --metadata disable-legacy-endpoints=true --scopes "https://www.googleapis.com/auth/devstorage.read_only","https://www.googleapis.com/auth/logging.write","https://www.googleapis.com/auth/monitoring","https://www.googleapis.com/auth/servicecontrol","https://www.googleapis.com/auth/service.management.readonly","https://www.googleapis.com/auth/trace.append" --num-nodes "3" --enable-stackdriver-kubernetes --no-enable-ip-alias --network "projects/i-portfolio-234111/global/networks/default" --subnetwork "projects/i-portfolio-234111/regions/us-central1/subnetworks/default" --addons HorizontalPodAutoscaling,HttpLoadBalancing --no-enable-autoupgrade --no-enable-autorepair

### Create the 🐳 image

* mvn -f search/pom.xml clean package -P k8s jib:build; and mvn -f ui/pom.xml clean package -P k8s jib:build
* https://console.cloud.google.com/gcr/images/i-portfolio-234111?project=i-portfolio-234111

### Deploy apps !

* kub apply -f ui/src/main/k8s/ui.v1.yaml

* Get the IP of the cluster: 

> kubectl get ingress first-ingress -o json | jq '.status.loadBalancer.ingress[].ip' -r

* Add to /etc/hosts the previous ip under `ui.demo-google-cloud.stack-labs.com`

> curl -qs ui.demo-google-cloud.stack-labs.com 

* Follow in a term the state of the pods | svc

> watch -n 1 "kubectl get po,deploy,svc"

* Deploy the `search` app v1

> kub apply -f search/src/main/k8s/search.v1.yaml

* Follow the behavior of the app

> watch -d -n .5 curl -qs ui.demo-google-cloud.stack-labs.com

* Scale up the search app

> kub apply -f search/src/main/k8s/search.v1.multi.yaml

* Move to v2 of Search

> kub apply -f search/src/main/k8s/search.v2.yaml

### All power of Google cloud in GKE

* https://console.cloud.google.com/traces/traces?project=i-portfolio-234111
* https://console.cloud.google.com/logs/viewer?project=i-portfolio-234111&minLogLevel=0&expandAll=false&timestamp=2019-03-14T21:58:33.826000000Z&customFacets=&limitCustomFacetWidth=true&dateRangeStart=2019-03-14T20:58:34.076Z&dateRangeEnd=2019-03-14T21:58:34.076Z&interval=PT1H&resource=k8s_container%2Fcluster_name%2Fk8s-demo-cluster%2Fnamespace_name%2Fdefault&scrollTimestamp=2019-03-14T21:43:25.522000000Z
